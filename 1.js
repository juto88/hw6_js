function createNewUser() {
    const newUser = {
      _firstName: "",
      _lastName: "",
      
      getAge: function() {
        const currentDate = new Date();
        const age = (currentDate - this._birthDate) / 31536000000; // количество миллисекунд в году
        return Math.floor(age); // округляем возраст до целого числа
      },
      createBirthday: function() {
        const birthday = prompt("Напишіть свою дату народження: dd.mm.yyyy");
            this._birthDate = new Date(birthday.split(".").reverse().join("-"));
            const age = this.getAge();
        console.log("Дата рождения:", birthday);
        console.log("Текущая дата:", new Date().toLocaleDateString());
        console.log("Возраст:", age);
      },
      set firstName(name) {
        console.log("Setting name is not allowed"); // Виводимо повідомлення про заборону
      },
      get firstName() {
        return this._firstName;
      },
      set lastName(surname) {
        console.log("Setting surname is not allowed");
      },
      get lastName() {
        return this._lastName;
      },
      get getLogin() {
        return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
      },
      get getPassword() {
        const birthYear = this._birthDate.getFullYear(); // Додано отримання року народження
        return (
            this._firstName[0].toUpperCase() +
            this._lastName.toLowerCase() +
            birthYear
            );
        },
    };
  
    newUser.setFirstName = function (name) {
      this._firstName = name;
    }
  
    newUser.setLastName = function (surname) {
      this._lastName = surname;
    }

    const name = prompt("Напішіть своє ім'я");
    const surname = prompt("Напішіть своє прізвище");
    newUser.setFirstName(name);
    newUser.setLastName(surname);
    newUser.createBirthday();
    console.log(newUser.firstName);
    console.log(newUser.lastName);
    console.log(newUser.getLogin);
    console.log(newUser.getPassword);
    
  };
  
  createNewUser();
